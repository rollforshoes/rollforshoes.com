const mdAnchor = require('markdown-it-anchor')

module.exports = (config) => {
  config.addPassthroughCopy('manifest.json')

  config.setTemplateFormats([
    'css',
    'html',
    'ico',
    'js',
    'md',
    'njk',
    'png',
    'svg'
  ])

  config.setServerOptions({
    domDiff: false
  })

  const markdown = require('markdown-it')({
    breaks: true,
    html: true,
    linkify: true
  }).use(mdAnchor)

  config.setLibrary('md', markdown)

  return {
    dir: {
      output: 'public'
    }
  }
}
