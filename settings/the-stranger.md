---
layout: main.njk
title: Scenario "The Stranger"
---

<h1 class="-scenario">The Stranger</h1>

The citizens of a small frontier town are visited by an ominous stranger.

## Introduction

> Somewhere along the dusty western frontier, there's a rough town just struggling to survive.
> - What's the name of this town?

> The town has built up from the first settlement in this area, after it drew crowds of folks for business opportunities, frontier adventure, and for some, the promise of a better future.
> - What industry is the town known for?

> Some would say the surrounding area is scenic. Others call the wilderness desolate.
> - What landmarks are nearby?

> Write down your character name and a brief description.

> You're all normal folks, citizens here, and of course you've got to make a living somehow.
> - Write down: What do you do in town?

> Now everyone's got their hobbies, their interests, their own way of spending time on the frontier.
> - Write down: What do you like to do in your spare time?

Have the players introduce their characters.

## Gameplay Segments

### A Typical Afternoon

The citizens are enjoying a calm, perfectly normal afternoon in town. Everyone's going about their business, doing their work, living as they do. Spend time highlighting the characters' interactions as they usually would, transitioning to the saloon as evening sets in. Give the characters some reason to head there, or at least be out along main street nearby.

### The Stranger's Arrival

The stranger arrives, dressed in all black, hat pulled down over his face, on a black horse and armed to the teeth. His first stop is the saloon, of course, and he's looking to pick a fight.

The characters ideally should each have some type of interaction with the stranger, whether it's a cursory glare or an outright argument. Establish the general unpleasantness that the stranger carries with him, and pick a particular player that bothers him the most. This character will be called out in a duel, to happen at high noon the following day.

After the conflict, play out the rest of the night and next morning as the mood in town has shifted substantially.

### High Noon

The citizens speak in hushed voices about the impending duel. Bets are placed, nervous glances exchanged, and crowds linger near main street. Again highlight the characters as they prepare for the duel.

The climax of the session is the duel itself. The targeted character should feel the pressure of the town on them. At high noon, the *expected* event will have the stranger square up against the chosen character in the middle of main street, where the two will have a stare-down before drawing and firing their weapons at one another. In reality, it's completely up to the players and the events leading up to now whether it will be that straightforward, but some resolution with the stranger should happen by the end.

If the stranger survives, he will calmly have a couple drinks in the saloon and leave town, riding off into the sunset.

## Random Tables

### Professions

*See [NPC Tables](/tools#npc-tables) for NPC adjectives.*

1. Blacksmith
2. Barkeep
3. Doctor
4. Prospector
5. Prostitute
6. Tailor
