---
layout: main.njk
title: Scenario "Kitchen Khaos"
---

<h1 class="-scenario">Kitchen Khaos</h1>

A group of courtly servants rush to prepare and serve a fantastical royal feast.

## Introduction

> You're the trod-upon servants of a king's court, tasked with staging a fittingly grand feast for a grand event.
> - What is the big occasion?
> - Why is the court so uptight about it?

> Unfortunately, preparations for the feast are dreadfully behind schedule, and the staff is now forced to throw together whatever it can manage at the last minute.
> - Why haven't you had enough time to prepare for the feast?
> - What's on the line if the feast doesn't go well?

> The now-rushed plans for the feast's preparation require a particularly elusive key ingredient.
> - What rare, nigh-mythical ingredient will you need?
> - What makes this critically important ingredient so hard to obtain?

> Each of you has a traditional courtly job, and in the rush to feed the royals, are now assigned a specific task for the event.
> - Write down your name, a description, your court position, and your feast task

## Gameplay Segments

### Gathering the Ingredients

The characters are gathered together by the King's Food Chancellor and given their tasks. They prep the ingredients and are pressured to do their tasks.

### If You Can't Stand the Heat...

The cooking begins, where some of the characters should be forced to participate in for some critical kitchen action.

### Dinner is Served

The court assembles to be served, and the quality of the prepared food and ensuing activities should be judged by the King and whomever else of stately status is in attendance.

## Random Tables

### Species

*Roll for adjectives on [NPC Tables](/tools#npc-tables).*

1. Dragonkin
2. Dwarf
3. Elf
4. Halfling
5. Human
6. Orc
