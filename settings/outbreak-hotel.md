---
layout: main.njk
title: Scenario "Outbreak Hotel"
---

<h1 class="-scenario">Outbreak Hotel</h1>

A ragtag group of survivors must make their way to safety following the outbreak of a highly contagious zombie-spawning virus.

## Introduction

> You all find yourselves at a popular resort hotel in a prime vacation destination.
> - Where is the hotel located?
> - (Optional) What is the name of the hotel?

This should inform you of the "theme" of the resort, and suggest available amenities, activities, etc. Start thinking of some fitting NPCs for the location.

> It's currently peak season at the resort, and it is packed with people. Each of you is here for a reason, whether you're on vacation, a business trip—or maybe you just work here and you're tired of the patrons. Write down your name, a brief description, and the reason for you being here.

The characters' details *should* help define the supporting cast—if they don't, prompt the players for more information to work with. Ideally, each character should be concerned about an NPC; having multiple player characters linked to one is even better.

> Rather unfortunately, each one of you also is utterly terrified of some rather common, everyday thing. Write that phobia down as well.

This is primarily just for added flavor, and should supply the group as a whole with ideas to expand details throughout the game for all the player characters. You can also consider giving characters a [flaw](/extras/#flaws) based on this fear.

## Gameplay Segments

### Checking In

The characters work their jobs at the hotel and should be borderline overwhelmed by how much activity is happening. With vacancies dwindling, the scenes should grow more chaotic. Throughout this first segment, subtle signs of the approaching zombie outbreak should be hinted at; NPCs coughing/sneezing, behaving oddly or irrationally, etc. to put the players on edge.

### The Outbreak

Guests at the hotel—along with any NPCs in the vicinity—begin turning into zombies as full-on chaos breaks out. The hotel is overrun, and the group of characters are forced together to survive and escape together. A few set piece action sequences should take place here, based on the chosen locale details and surrounding hazards.

### Escape

Again depending on the locale, and largely on the players themselves, a means of escape should be evident as the prime option for the group's safety. Use NPCs to suggest routes and move the group along, but emphasize the zombie problem by exacting horrific ends on them for the most part. Use any of the characters' remaining supporting cast from the intro to add drama. Give the players final actions to successfully escape, and heavily punish or reward the results.

## Random Tables

### Hotel Occupations

*Roll for adjectives on [NPC Tables](/tools#npc-tables).*

1. Bellhop
2. Housekeeper
3. Manager
4. Pool maintenance person
5. Receptionist
6. Valet
