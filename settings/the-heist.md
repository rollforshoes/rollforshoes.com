---
layout: main.njk
title: Scenario "The Heist"
---

<h1 class="-scenario">The Heist</h1>

A team of supposedly skilled sleuths come up with and enact a plan to steal something from somewhere.

## Introduction

> You are all part of a heist team, focused on stealing a certain treasured item. It's something of high value, or of great importance to some of you.
> - What is the treasured item?

> The treasured item is appropriately highly coveted, and is somehow secured away. The surrounding locale will be hard to get into, and will require careful planning.
> - Where is the item located?

> Write down your character name and a brief description.

> Each of you has joined the team and been given an important role.
> - Write down: What job are you assigned on the team?

> Likewise, each of you has your own reason for joining the team, and your own ideal that you're striving for as the team assembles.
> - Write down: What do you want out of obtaining the item?

> Finally, with the target carefully chosen and the planning underway, you've been assigned a task in preparation for the heist.
> - Write down: What is your established connection to the target tied to the task?

Have each player introduce their character and their answers to the questions, asking for follow-up details to better flesh out the group as a whole.

Depending on how large the group is, it might make sense to have some other NPCs as part of the heist team. Roll on [NPC Tables](/tools#npc-tables) and the below [NPC Nouns](#npc-nouns) to assemble other characters, but have them primarily support the PCs in the narrative.

## Gameplay Segments

### Planning and Recon

This is the characters' initial meeting to establish the plan to steal the item. Connections, obstacles, positioning, etc. is all setup here, in character as much as possible. Prompt the players with possible issues relevant to the location.

Details about the characters' connections, in the form of NPCs, locales, and equipment should give you enough information to assemble some form of heist outline. Ask enough questions and roleplay the scene(s) to make sure you've got enough to work with once the action starts.

### Action!

The plan is put into motion. Each character is given a chance to move the plan forward, using the discussed obstacles and NPCs. The treasured item should be the goal at this point. A *twist* is also good to throw in towards the end of this segment; come up with one or roll on the [Twists](#twists) table below.

### The Escape

Whether the item has been successfully taken or not, the pressure on the characters should be escalating enough that the characters want to escape. Bring back any relevant obstacles and/or NPCs that would cause trouble for the team, and aim to end the session on a cliffhanger or a dramatic set piece.

## Random Tables

### Professions

*Roll for Description and Personality in [NPC Tables](/tools#npc-tables).*

1. Law enforcement
2. Charismatic "expert"
3. Stuntperson
4. Lockpicker
5. Self-proclaimed master of disguise
6. Loudmouth jerk

### Twists

1. The item is in another location
2. A double-cross: one of the team members is working against the team
3. A trap is sprung, capturing at least one of the team members
4. Another thief or team of thieves is discovered going after the item
5. The item is not what it seems
6. The authorities get the jump on the team
