---
layout: main.njk
title: Scenario "Redshirt Recon"
---

<h1 class="-scenario">Redshirt Recon</h1>

A Spacefleet away team heads planetside to investigate a distress signal.

## Introduction

> You are all members of the prestigious Spacefleet, and part of the crew of a starship travelling through space. You've received a weak distress signal on a nearby planet.
> - What type of planet is it?

> It's standard Spacefleet protocol to investigate distress signals for aid, but there are a couple red flags.
> - What makes the crew hesitant to investigate?
> - Despite the hesitation, what finally convinces the Captain to send a team?

> Write down your character name and a brief description.

> Each of you normally has an fairly menial assigned job on the ship.
> - Write down: What lowly job do you usually have on the ship?

> The away team is formed from crew members, each with a useful attribute.
> - Write down: What makes you a good choice for this mission?

> Finally, each away team member is given a specific task and relevant gear.
> - Write down: What critical piece of equipment have you been given as your assignment?

Have everyone introduce their character and information they've written down, asking for further details when necessary and encouraging others to do the same.

The team will benefit from having someone in charge on the mission, likely a Chief Officer or equivalent (it's rare for the Captain to go, but not impossible). This should be an NPC, so roll on [NPC Tables](/tools#npc-tables) to assemble a character.

## Gameplay Segments

### Mission Briefing

The away team is assembled, red shirts handed out, and the mission outlined. The team members should speak freely about what obstacles they might find, giving you more information to work with once they're on the surface. A goal should be very clearly established for what to do once the signal source is found, and the mission begins.

### Planet Exploration

The team explores the surface of the planet, tracking down the source of the distress signal. Depending on what type of planet the group came up with, this exploration could involve nearly anything. Use any [Random Tables](#random-tables) below to roll details as needed.

The team leader should keep the goal focused on finding the signal source, but there could be any number of distractions along the way. Use obstacles that were brought up in the briefing discussion, and encourage the players to use their equipment for their assigned task(s).

### Source Encounter

Once the source is located, there's a final encounter. The "red flag" brought up in the Introduction should be used in some way. If you're not sure what the signal source is at this point, roll on the [Signal Sources](#signal-sources) table below. The [Dangers](#dangers) table can also be used to threaten the team.

The encounter should be difficult, using skills the team has gained and capitalizing on weaknesses that have been established. The end of the session should be a dramatic resolution of the discoveries.

## Random Tables

<div class="row">
<section>

### Dangers

1. Aggressive wildlife
2. A corrupt former Spacefleet Admiral
3. A rogue, violent android
4. Environmental disaster
5. Attack from local primitive species
6. Evil clones of the away team

</section>
<section>

### Species

1. Robot
2. Reptilian alien
3. Insectoid alien
4. Sentient plant
5. Mutant
6. Space pirate

</section>
<section>

### Signal Sources

1. A crashed ship
2. A failed colony
3. A trap laid by enemies
4. A creature that is emitting the signal
5. A space anomaly that is emitting the signal
6. An otherworldly artifact

</section>
</div>
