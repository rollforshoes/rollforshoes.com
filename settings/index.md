---
layout: main.njk
title: Settings
---

## Adventures

- [The Night Before Christmas](https://sasswrites.itch.io/the-night-before-christmas) by [SassWrites](https://sasswrites.itch.io)

## Scenarios

The following can be used as starting points for games, each outlined in the same way:

- *Introduction:* questions and initial prompts to setup the scenario
- *Gameplay Segments:* narrative blocks to guide phases of the game
- *Random Tables:* tables to roll on for data relevant to the setting

#### [The Heist](/settings/the-heist)
*Skilled sleuths enact a plan for a big score.*

#### [Kitchen Khaos](/settings/kitchen-khaos)
*Courtly servants rush to prepare and serve a fantastical feast.*

#### [Minimon: Miniature Monsters](/settings/minimon)
*A Minimon team helps track down and capture an elusive one of their own.*

#### [Outbreak Hotel](/settings/outbreak-hotel)
*Busy staff are faced with the abrupt arrival of a novel zombie virus.*

#### [Redshirt Recon](/settings/redshirt-recon)
*A Spacefleet away team heads planetside to investigate a distress signal.*

#### [The Stranger](/settings/the-stranger)
*Citizens of a wild west frontier town are visited by an ominous stranger.*
