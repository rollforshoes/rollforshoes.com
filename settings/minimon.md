---
layout: main.njk
title: Scenario "Minimon"
---

<h1 class="-scenario">Minimon: Miniature Monsters</h1>

A team of *miniature monsters* helps their Coach track down and capture a rare, elusive monster.

## Introduction

> You're a team of Minimon, helping your Coach collect and catalogue the planet's diverse array of your fellow creatures. Your Coach has nearly got 'em all—save for one last, elusive Minimon.
>
> - What is the final Minimon (name and description)?
> - Why is it so hard to find?
> - How has it evaded capture so effectively?

> Every Minimon has a unique resemblance to an animal or object, and most have some kind of elemental affinity. Write down the following:
>
> - Your Minimon species name and description
> - How you first encountered your Coach
> - An element type you're aligned with, as a level 2 skill
> - An element type that you're weak against, as a level (-2) defensive [flaw](/extras/#flaws) (e.g., *Fire Defense -2*)
> - Your signature Move, as a level 3 skill

## Gameplay

### I Choose You!

Determine a location by rolling on the [Locations](#locations) table below.

Start *in medias res,* with a fight in progress against a trainer and one of their Minimon. The Coach should summon each player Minimon, letting them shine and highlighting their abilities, dealing narrative damage to the opposing side.

### Missile Squad

With the trainer fight finished, the team unfortunately encounters the problematic *Missile Squad,* a group of greedy villains that aims to exploit Minimon for their own gain.

Create a pair of bully trainers (or roll on [NPC Tables](/tools/#npc-tables)), accompanied by a mean Minimon of their own (roll on [Forms](#minimon-forms) and [Types](#minimon-types) below).

This encounter isn't necessarily a fight—the Missile Squad's interference with the team should be more of a challenge that tests the problem-solving of the members.

### The Legendary

With the Missile Squad dealt with, the team finally faces the Legendary Minimon alluded to in the setup. The location should be a mix between the Legendary's natural domain and the location leading to it, and the established difficulty of finding it already surmounted at this point.

Don't hold back on this encounter: the Legendary should be a challenge for everyone to overcome. The Coach will try to catch this Minimon, while the player Minimon focus on attacking, defending, and trying to ensnare the last Legendary.

## Random Tables

<div class="row">
<section>

### Locations

1. Slimy swamp
2. Snowy mountain
3. Spooky cave
4. Sunny beach
5. Tangled forest
6. Windy desert

</section>
<section>

### Minimon Forms

1. Bird
2. Blob
3. Canine
4. Feline
5. Insect
6. Rodent

</section>
<section>

### Minimon Types

<div class="row">
<section>

#### 1–3

1. Bug
2. Electric
3. Fighting
4. Fire
5. Grass
6. Ground

</section>
<section>

#### 4–6

1. Ice
2. Metal
3. Poison
4. Psychic
5. Rock
6. Water

</section>
</section>
</div>
