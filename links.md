---
layout: main.njk
title: Links
---

## Communities

- [Discord server](https://discord.gg/NyRsWXEaVH)
- [Reddit: r/rollforshoes](https://www.reddit.com/r/rollforshoes/)
- [Stack Exchange](https://rpg.stackexchange.com/questions/tagged/roll-for-shoes)

## Actual Plays

Listen and/or watch folks playing *Roll for Shoes*!

### Podcasts

- [City on a Hill Gaming](https://cityonahillgaming.com): [The World's Largest Grocery Store: A Product-Placement Podcast](https://cityonahillgaming.com/episode/the-worlds-largest-grocery-store-a-product-placement-podcast)
- [City on a Hill Gaming](https://cityonahillgaming.com): [The World's Largest Grocery Store, Part 2: I Chose That Skill Name Very Carefully](https://cityonahillgaming.com/episode/the-worlds-largest-grocery-store-part-2-i-chose-that-skill-name-very-carefully)
- [City on a Hill Gaming](https://cityonahillgaming.com): [A Very Tiny Trip to the World's Largest Grocery Store: "Lettuce Turnip the Beat"](https://cityonahillgaming.com/episode/-)
- [Geeks at Play Episode 1: Roll for Shoes and Big Muppet Heroes](https://geekatarms.podbean.com/e/geeks-at-play-episode-1-roll-for-shoes-and-big-muppet-heroes/)
- [Geeks at Play Episode 2: Cleanup at NutriCorp](https://geekatarms.podbean.com/e/geeks-at-play-episode-2-cleanup-at-nitricorp/)

### Youtube

- Gamebound: [Roll for Shoes](https://www.youtube.com/playlist?list=PLy4ROgY6jK7FkOTojQXcUngrd3RhzdH6Q) *(3:56:55)*
- Into the Dungeon: [I Roll for Shoes | One-shot](https://youtu.be/MtsD4WbsWcY?si=KAWAW-_138GBeNhW) *(5:03:58)*
- PlayingBoardGames: Roll for Fan Service [Part 1](https://www.youtube.com/watch?v=KBBnK0041bU) *(35:10)* [Part 2](https://www.youtube.com/watch?v=FJl47xi0raY) *(31:51)* [Part 3](https://www.youtube.com/watch?v=TlSuv57a3SQ) *(30:59)* [Part 4](https://www.youtube.com/watch?v=wDxpukn2M7Q) *(31:13)* [Part 5](https://www.youtube.com/watch?v=QbGtZkk1Wec) *(44:36)*
- Saving Throw: [Barky's Brigade episode](https://www.youtube.com/watch?v=IpMZ_XZnjzE&list=PLkHjiWAUZQbqRIMnO3bj7Vt6aUGt6CNse&index=27) *(3:14:10)*
- Saving Throw: [Special Event](https://www.youtube.com/live/znzYbK3TDUo?si=A4f96lNujWFQ9VaP) *(3:38:10)*
- Saving Throw: ["Carnie Carnage"](https://www.youtube.com/playlist?list=PLkHjiWAUZQbohFc2-ZEKh-YP_00LOpMjC) *(2:28:23)*
- Saving Throw: [Holiday Spectacular](https://youtu.be/g-lw4Cvc1qM?si=AcLbMTS_1Rd4l9Cp) *(3:06:31)*
- Saving Throw: ["The Imps of Hell"](https://www.youtube.com/playlist?list=PLkHjiWAUZQboeU43utTkHbOMwKLIaXlAy) *(2:52:23)*
- Saving Throw: [RPG Exploration Society episode](https://youtu.be/hMXHpi9qfFY?si=sNMVD0XLI-fmuF7k) *(3:07:20)*
- Saving Throw: [ALZ Marathon 2017](https://youtu.be/1UHxcnSj4G4?si=04nl5W2DgnrdggxV) *(3:58:00)*
- Saving Throw: [NPC Game Unlock](https://youtu.be/nVLwP0d-WRo?si=uwKXKqvdVTPtva7-) *(3:20:54)*
- Saving Throw: [Spring Break 2019](https://youtu.be/nDVeEZxRxLU?si=6jPGGjsuluJRoOyY) *(2:57:59)*
