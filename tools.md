---
layout: main.njk
title: Tools
---

# Tools

## General Body Parts

1. Head
2. Left arm
3. Right arm
4. Torso
5. Left leg
6. Right leg

## NPC Tables

Roll D66 (two D6 side by side) for each of the tables below.

### <span>Description</span> <button>Roll</button>

|                 |                |                 |
| --------------- | -------------- | --------------- |
| `11` Adorable   | `31` Filthy    | `51` Slender    |
| `12` Attractive | `32` Furry     | `52` Slimy      |
| `13` Bald       | `33` Glamorous | `53` Spiky      |
| `14` Bearded    | `34` Huge      | `54` Stinky     |
| `15` Beefy      | `35` Lanky     | `55` Stylish    |
| `16` Bony       | `36` Muscular  | `56` Sunburned  |
| `21` Bulky      | `41` Obese     | `61` Tall       |
| `22` Chiseled   | `42` Pasty     | `62` Tattooed   |
| `23` Chubby     | `43` Petite    | `63` Tentacled  |
| `24` Clean      | `44` Scary     | `64` Tiny       |
| `25` Creepy     | `45` Shifty    | `65` Ugly       |
| `26` Elderly    | `46` Short     | `66` Voluptuous |

### <span>Personality</span> <button>Roll</button>

|                   |                  |                    |
| ----------------- | ---------------- | ------------------ |
| `11` Annoying     | `31` Fearless    | `51` Noisy         |
| `12` Arrogant     | `32` Fidgety     | `52` Optimistic    |
| `13` Awkward      | `33` Friendly    | `53` Quiet         |
| `14` Bossy        | `34` Grumpy      | `54` Rowdy         |
| `15` Clumsy       | `35` Judgemental | `55` Rude          |
| `16` Confident    | `36` Kind        | `56` Sad           |
| `21` Courageous   | `41` Lazy        | `61` Sarcastic     |
| `22` Demanding    | `42` Maniacal    | `62` Selfish       |
| `23` Embarrassed  | `43` Mean        | `63` Shy           |
| `24` Enthusiastic | `44` Messy       | `64` Silly         |
| `25` Evil         | `45` Murderous   | `65` Simple-Minded |
| `26` Excited      | `46` Nervous     | `66` Stubborn      |

<script>
window.addEventListener('click', (event) => {
  if (event.target.nodeName !== 'BUTTON') return

  const table = event.target.parentElement.nextElementSibling
  let cells = Array.from(table.querySelectorAll('td'))

  const selectedCell = table.querySelector('td.-selected')
  if (selectedCell) {
    selectedCell.classList.remove('-selected')
    cells = cells.filter((c) => c !== selectedCell)
  }

  const cell = cells[Math.floor(Math.random() * cells.length)]
  cell.classList.add('-selected')
})
</script>
