---
layout: main.njk
---

<style>
  main .content {
    padding-bottom: var(--huge);
  }
</style>

**Roll for Shoes** is a tabletop RPG "micro system" [created by *Ben Wray*](https://web.archive.org/web/20200619145843/http://story-games.com/forums/discussion/11348/microdungeons-i-roll-to-see-if-i-have-shoes-on), with these simple rules:

1. Say what you do and roll a number of D6s, determined by the level of relevant skill you have.
2. If the sum of your roll is higher than an opposing roll, the thing you wanted to happen, happens.
3. At start, you have only one skill: *Do Anything 1*.
4. If you roll all 6s, you get a new skill specific to the action, one level higher than the one you used.
5. For every roll you fail, you get 1 XP.
6. XP can be used to change a die into a 6 for advancement purposes only.
