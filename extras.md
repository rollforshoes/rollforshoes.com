---
layout: main.njk
title: Extras
---

## Advancement

New skills should be more specific than the skill rolled, and relevant to the action taken.

In rule 6, "for advancement purposes only" means you can spend XP to gain a new skill by upgrading dice, but the narrative result of the action still stands as rolled.

After you fail a roll, you can immediately use the gained XP for a new skill.

## Difficulty

When opposition is not from another character, the GM rolls a number of dice based on how difficult a task is:

<div class="inline-list">
  <span><em>1d6</em>Easy</span>
  <span><em>2d6</em>Average</span>
  <span><em>3d6</em>Hard</span>
  <span><em>4d6</em>Nearly impossible</span>
</div>

*Alternatively,* the GM can just pick a target number to beat, generally between 3 and 18. You can use these four reference points:

<div class="inline-list">
  <span><em>3</em>Easy</span>
  <span><em>7</em>Average</span>
  <span><em>10</em>Hard</span>
  <span><em>14</em>Nearly impossible</span>
</div>

## Ties

When your result is a tie with the opposing roll, you both **partially succeed *and* fail**. You don't get XP, and accomplish your goal *barely* with an unexpected twist.

## Statuses

A *status* is a modifier that affects actions, positively or negatively. They're written with their rating first, and modify the results of any related actions.

> For example, if you had stayed awake for a couple days straight, you might suffer *-3 Tired*. When you perform an especially exhaustive action, like *Climbing 2*, you'd roll your normal 2 dice—say you rolled a 3 and a 6, totaling 9—then subtract 3 for being *Tired,* to get a final total **6**.

A status will remain on a character until it's removed, either by action or otherwise made narratively untrue.
Advancement still works as normal with statuses, based on the rolled dice.

You can use a status to track many things:

- Harm or damage, rated by the result of the inflicting action
- Temporary conditions, like being *Poisoned* or *Highly Caffeinated*
- Progress: a craft project, quality of an item, etc.

## Skill Slots

For longer campaigns, or games where you want to restrict advancement, consider limiting skills to a total number of *slots* per level. Players then must choose to replace skills when gaining new ones.

The following skill "triangle" is a good starting shape:

- Four level 2 skills
- Three level 3 skills
- Two level 4 skills

### Leveling

For even longer game support, you can allow purchasing skill slots for XP, at a cost of twice the level. This cost simply makes the slot available, and while it is separate from obtaining a skill (done normally), players can buy a skill slot at the same time they obtain a skill.

## Starting Skills

Depending on the scenario, characters could be customized and have additional starting skills. These could include broad archetypes/classes, specific skills for the scenario, etc.

You could go a step further, and **replace** *Do anything 1* with common broad skills, akin to attributes in other systems, and have a number of points to assign them.

> For example, a GM wants to run a campaign with attributes *Strength*, *Mind*, and *Will*. Each character begins with these three "skills" first, and six points to put into them—rating each 1, 2, or 3.

## Flaws

It's possible to be especially *bad* at something, called a flaw, and written as a negative skill. When you act with a relevant flaw, you roll the number of dice indicated (as absolute value) and **only keep the lowest result**. Good luck!
